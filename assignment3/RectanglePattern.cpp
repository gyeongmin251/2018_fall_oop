#include "RectanglePattern.h"
#include <iostream>
using namespace std;

RectanglePattern::RectanglePattern(){
    
}

RectanglePattern::RectanglePattern(int x,int y){
    length=x;
    width=y;
}
void RectanglePattern::set_length(int x){
    length=x;
}
void RectanglePattern::set_width(int y){
    width=y;
}
int RectanglePattern::patternHelper(int l,int w){
    if(w<0){
        
        return 0;
    }else{
        for(int i=0;i<l;i++){
        cout<<"*";
        }
        cout<<endl;
        return patternHelper(l,w-1);
    }
}
void RectanglePattern::printPattern(){
    if(length<=0){
        cout<<"\nInvalid size!\n"<<endl;
    }else{
    cout<<"The Rectangle Pattern:(length = "<<length<<": width = "<<width<<endl;
    patternHelper(length,width);
    }
}
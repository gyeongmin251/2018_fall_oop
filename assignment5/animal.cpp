#include <iostream>
#include "animal.h"
#include <fstream>
#include <cctype>
using namespace std;

ifstream inFileDog;
ifstream inFileFish;
ifstream inFileHorse;
ifstream inFileMonkey;
ifstream inFileLizard;

Dog::Dog(){

}
void Dog::readInfo(){
	string breed, color, name,expn="Dog is not correct insert\n";
	int age, weight;

	try{
 		inFileDog.open("/home/arabica251/Downloads/8주차_csv/Dog.csv");
		getline(inFileDog, name, '\n');
		getline(inFileDog, name, ',');
		getline(inFileDog, breed, ',');
		inFileDog >> age;
		getline(inFileDog, color, ',');
		getline(inFileDog, color, ',');
		inFileDog >> weight;
		
		if (!isalpha(name.at(0)) && !isalpha(color.at(0)) && !isalpha(breed.at(0))){
			throw expn;
		}
		setAge(age);
		setName(name);
		setColor(color);
		setBreed(breed);
		setWeight(weight);

	}
	catch (string expn){
		cout << expn;
	}
}
void Dog::printInfo(){
	cout << "Dog Information\n" << "Name : " << getName() << endl << "Breed : " << getBreed() << "\nAge : " << getAge() << " (years)\nColor : " << getColor() << "\nWeight : " << getWeight() << " (pounds)" << endl;
}
void Dog::subtractTen(){
	weight = weight - 10;
}

Fish::Fish(){

}
void Fish::readInfo(){
	string habitat, color, name, expn = "Fish is not correct insert\n";
	string freshwater, predator;
	try{
		inFileFish.open("/home/arabica251/Downloads/8주차_csv/Fish.csv");
		getline(inFileFish, name, '\n');
		getline(inFileFish, name, ',');
		getline(inFileFish, color, ',');
		getline(inFileFish, freshwater, ',');
		if (freshwater.compare("TRUE") == 0){
			setFreshwater(true);
		}
		else if (freshwater.compare("FALSE") == 0){
			setFreshwater(false);
		}
		else{
			throw expn;
		}
		getline(inFileFish, habitat, ',');
		getline(inFileFish, predator, '\n');
		if (predator.compare("TRUE") == 0){
			setPredator(true);
		}
		else if (predator.compare("FALSE") == 0){
			setPredator(false);
		}
		else{
			throw expn;
		}
		if (!isalpha(name.at(0)) && !isalpha(color.at(0)) && !isalpha(habitat.at(0))){
			throw expn;
		}
		setName(name);
		setColor(color);
		setHabitat(habitat);

	}
	catch (int expn){
		cout << expn;
	}
}
string Fish::convertBool(bool value){
	if (value == true){
		return "TRUE";
	}
	else{
		return "FALSE";
	}
}
void Fish::printInfo(){
	string temp1, temp2;
	
	if (getPredator() != 0){
		temp2 = "TRUE";
	}
	else{
		temp2 = "FALSE";
	}
	cout << "Fish Information\n" << "Name : " << getName() << endl << "Color : " << getColor() << "\nFreshwater : " << convertBool(getFreshwater()) << " \nHabitat : " << getHabitat() << "\nPredator : " << convertBool(getPredator()) << endl;
}
Horse::Horse(){

}
void Horse::readInfo(){
	string manecolor,temp, name, color, expn = "Horse is not correct insert\n";
	int height, age;
	try{
		inFileHorse.open("/home/arabica251/Downloads/8주차_csv/Horse.csv");
		getline(inFileHorse, name, '\n');
		getline(inFileHorse, name, ',');
		getline(inFileHorse, color, ',');
		getline(inFileHorse, manecolor, ',');
		inFileHorse >> age;	
		getline(inFileHorse, temp, ',');
		inFileHorse >> height;
		if (!isalpha(name.at(0)) && !isalpha(color.at(0)) && !isalpha(manecolor.at(0))){
			throw expn;
		}
		setName(name);
		setAge(age);
		setColor(color);
		setManeColor(manecolor);
		setHeight(height);
	}
	catch (int expn){
		cout << expn;
	}
}
void Horse::printInfo(){
	cout << "Horse Information\n" << "Name : " << getName() << endl << "Color : " << getColor() << "\nMane Color : " << getManeColor() << " \nAge : " << getAge() << "( years)\nHeight : " << getHeight() << " (hands)" << endl;
}
Lizard::Lizard(){

}
void Lizard::readInfo(){
	string habitat, isprotected, name, color, expn = "Lizard is not correct insert\n";
	int weight;
	try{
		inFileLizard.open("/home/arabica251/Downloads/8주차_csv/Lizard.csv");
		getline(inFileLizard, name, '\n');
		getline(inFileLizard, name, ',');
		getline(inFileLizard, color, ',');
		getline(inFileLizard, habitat, ',');
		getline(inFileLizard, isprotected, ',');
		if (isprotected.compare("TRUE") == 0){
			setProtected(true);
		}
		else if (isprotected.compare("FALSE") == 0){
			setProtected(false);
		}
		else{
			throw expn;
		}
		inFileLizard >> weight;
		if (!isalpha(name.at(0)) && !isalpha(color.at(0)) && !isalpha(habitat.at(0))){
			throw expn;
		}
		setName(name);
		setColor(color);
		setHabitat(habitat);
		setWeight(weight);
	}
	catch (int expn){
		cout << expn;
	}
}
void Lizard::printInfo(){
	cout << "Lizard Information\n" << "Name : " << getName() << endl << "Color : " << getColor() << "\nHabitat : " << getHabitat() << " \nProtected : " << convertBool(getProtected()) << "\nWeight : " << getWeight() << " (ounces)" << endl;
}
string Lizard::convertBool(bool value){
	if (value == true){
		return "TRUE";
	}
	else{
		return "FALSE";
	}
}
Monkey::Monkey(){

}
void Monkey::readInfo(){
	string home, wild, name, color, endangered, expn = "Monkey is not correct insert\n";
	int age;
	try{
		inFileMonkey.open("/home/arabica251/Downloads/8주차_csv/Monkey.csv");
		getline(inFileMonkey, name, '\n');
		getline(inFileMonkey, name, ',');
		getline(inFileMonkey, color, ',');
		inFileMonkey >> age;
		getline(inFileMonkey, home, ',');
	
		getline(inFileMonkey, wild, ',');
		if (wild.compare("TRUE") == 0){
			setWild(true);
		}
		else if (wild.compare("FALSE") == 0){
			setWild(false);
		}
		else{
			throw expn;
		}
		getline(inFileMonkey, home, ',');
		getline(inFileMonkey, endangered, '\n');
		if (endangered.compare("TRUE") == 0){
			setEndangered(true);
		}
		else if (endangered.compare("FALSE") == 0){
			setEndangered(false);
		}
		else{
			throw expn;
		}
		if (!isalpha(name.at(0)) && !isalpha(color.at(0)) && !isalpha(home.at(0))){
			throw expn;
		}
		setHome(home);
		setName(name);
		setColor(color);
		setAge(age);
	}
	catch (int expn){
		throw expn;
	}
}
void Monkey::printInfo(){
	cout << "Monkey Information\n" << "Name : " << getName() << endl << "Color : " << getColor() << "\nAge : " << getAge() << " \nWild : " << convertBool(getWild()) << "\nHome : " << getHome() << "\nEndangered : " << convertBool(getEndangered())<<endl;
}
string Monkey::convertBool(bool value){
	if (value == true){
		return "TRUE";
	}
	else{
		return "FALSE";
	}
}
void Monkey::changeEndangered(){
	if (getEndangered() == true){
		setEndangered(false);
	}
	else{
		setEndangered(true);
	}
}
#include <string>
#ifndef animal_h
#define animal_h
using namespace std;
class Animal{
    private:
    string name;
    string color;
    public:
    Animal(){
        name="";
        color="";
    }
    string getName(){
        return name;
    }
    string getColor(){
        return color;
    }
    void setName(string _name){
        name = _name;
    }
    void setColor(string _color){
        color=_color;
    }
    virtual void readInfo()=0;
    virtual void printInfo()=0;
};
#endif
#ifndef DOG_H
#define DOG_H
class Dog : public Animal{
    private:
    string breed;
    int age;
    int weight;
    public:
    Dog();
    string getBreed(){
        return breed;
    }
    int getAge(){
        return age;
    }
    int getWeight(){
        return weight;
    }
    void setBreed(string _breed){
        breed = _breed;
    }
    void setAge(int _age){
        age = _age;
    }
    void setWeight(int _weight){
        weight = _weight;
    }
    void readInfo();
    void printInfo();
    void subtractTen();
};
#endif
#ifndef FISH_H
#define FISH_H
class Fish : public Animal{
    private:
    bool freshwater;
    string habitat;
    bool predator;
    public:
    Fish();
    bool getFreshwater(){
        return freshwater;
    }
    string getHabitat(){
        return habitat;
    }
    bool getPredator(){
        return predator;
    }
    void setFreshwater(bool _freshwater){
        freshwater = _freshwater;
    }
    void setHabitat(string _habitat){
        habitat = _habitat;
    }
    void setPredator(bool _predator){
        predator = _predator;
    }
    void readInfo();
    void printInfo();
    string convertBool(bool _value);
};
#endif
#ifndef HOURSE_H
#define HOURSE_H
class Horse : public Animal{
    private:
    string maneColor;
    int height;
    int age;
    public:
    Horse();
    string getManeColor(){
        return maneColor;
    }
    int getHeight(){
        return height;
    }
    int getAge(){
        return age;
    }
    void setManeColor(string _maneColor){
        maneColor = _maneColor;
    }
    void setHeight(int _height){
        height=_height;
    }
    void setAge(int _age){
        age=_age;
    }
    void readInfo();
    void printInfo();
    void addOne(){
        height +=1;
    }
};
#endif
#ifndef LIZARD_H
#define LIZARD_H
class Lizard : public Animal{
    private:
    bool isProtected;
    string habitat;
    int weight;
    public:
    Lizard();
    string getHabitat(){
        return habitat;
    }
    int getWeight(){
        return weight;
    }
    bool getProtected(){
        return isProtected;
    }
    void setHabitat(string _habitat){
        habitat = _habitat;
    }
    void setWeight(int _weight){
        weight=_weight;
    }
    void setProtected(int _isProtected){
        isProtected=_isProtected;
    }
    void readInfo();
    void printInfo();
    string convertBool(bool _value);
};
#endif
#ifndef MONKEY_H
#define MONKEY_H
class Monkey : public Animal{
    private:
    bool wild;
    string home;
    bool endangered;
    int age;
    public:
    Monkey();
    string getHome(){
        return home;
    }
    int getAge(){
        return age;
    }
    bool getEndangered(){
        return endangered;
    }
    bool getWild(){
        return wild;
    }
    string setHome(string _home){
        home = _home;
    }
    void setAge(int _age){
        age=_age;
    }
    void setEndangered(bool _endangered){
        endangered=_endangered;
    }
    void setWild(bool _wild){
        wild=_wild;
    }
    void readInfo();
    void printInfo();
    string convertBool(bool _value);
    void changeEndangered();
};
#endif
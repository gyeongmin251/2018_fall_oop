#include <iostream>
#include <string>
using namespace std;
template<typename T>
T less(T v1,T v2){
    cout<<"gerneric"<<endl;
    return v1<v2?v1:v2;c
}
template<typename T>
T less(T* v1,T* v2){
    cout<<"partial"<<endl;
    return *v1<*v2?*v1:*v2;
}
int main(int argc,const char* argv[]){
    int x=25;
    int* pX=&x;
    int y=30;
    int* pY=&y;

    cout<<less(pY,pX)<<endl;
    cout<<less(10,2)<<endl;
    cout<<less(2.8,2.9)<<endl;
    cout<<less("qwe","asd123")<<endl;
    return 0;
}
#include <iostream>
#include "mathTeacher.h"
#include <string>

using namespace std;

mathTeacher::mathTeacher(string name,int age):person(name,age){
    setProfession(name);
    setAge(age);
}
void mathTeacher::teacherMath(){
    cout<<"I can teach Maths"<<endl;
}
#include "Sundae.h"
#include "Icecream.h"
#include <string>
#include <iostream>

using namespace std;

Sundae::Sundae(double cost,bool cone,int number,string dessertName):IceCream(cone,number,dessertName){
    setCostOfTopping(cost);
    setType(cone);
    setNumberOfScoops(number);
    setName(dessertName);
}
void Sundae:: setCostOfTopping(double cost){
    costOfTopping=cost;
}
double Sundae:: getCostOfTopping(){
    return costOfTopping;
}
void Sundae:: print(){
    cout<<getName()<<endl<<getNumberOfScoops()<<" scoops + "<<getCostOfTopping()<<" toppings"<<endl;
}
double Sundae:: getCost(){
    return getNumberOfScoops()*getCostOfTopping();
}
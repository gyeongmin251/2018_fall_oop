#include "Candy.h"
#include <string>
#include <iostream>

using namespace std;

Candy::Candy(double weight, double pricePerPound, string dessertName):dessertItem(dessertName){
	setWeight(weight);
	setPrice(pricePerPound);
	setName(dessertName);
}
void Candy::setWeight(double weight){
	candyWeight = weight;
}
double Candy::getWeight(){
	return candyWeight;
}
void Candy::setPrice(double price){
	pricePerPound = price;
}
double Candy::getPrice(){
	return pricePerPound;
}
void Candy::print(){
	cout <<getName()<<endl<< getWeight() << "pounds @" << getPrice()<<" per pound"   << endl;
}
double Candy::getCost(){
	return getPrice()*getWeight();
}
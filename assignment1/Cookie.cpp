#include "Cookie.h"
#include <string>
#include <iostream>

using namespace std;

Cookie::Cookie(int number,double price,string dessertName):dessertItem(dessertName){
    setNumberOfCookies(number);
    setPricePerDozen(price);
    setName(dessertName);
}
double Cookie:: getNumberOfCookies(){
    return numberOfCookies;
}
void Cookie:: setNumberOfCookies(int number){
    numberOfCookies=number;
}
void Cookie:: setPricePerDozen(double price){
    pricePerDozen=price;
}
double Cookie:: getPricePerDozen(){
    return pricePerDozen;
}
void Cookie:: print(){
    cout<<getName()<<endl<<getNumberOfCookies()<<" cookies @"<<getPricePerDozen()<<" per dozen"<<endl;
}
double Cookie:: getCost(){
    return getNumberOfCookies()/12*getPricePerDozen();
}